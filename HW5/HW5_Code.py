# -*- coding: utf-8 -*-

# Q6

import numpy as np
import matplotlib.pyplot as plt


rng = np.random.RandomState(1)
X = np.array([(1,1), (1,3), (2, 2), (2, 4), (3, 3), (3, 5), (4, 4), (4, 6), (5, 5), (5, 7)])
plt.scatter(X[:, 0], X[:, 1])
plt.axis('equal');

##
from sklearn.decomposition import PCA
pca = PCA(n_components=2)
pca.fit(X)

print(pca.components_)
print(pca.explained_variance_)

#%%
def draw_vector(v0, v1, ax=None):
    ax = ax or plt.gca()
    arrowprops=dict(arrowstyle='->',
                    linewidth=2,
                    shrinkA=0, shrinkB=0)
    ax.annotate('', v1, v0, arrowprops=arrowprops)
    

# plot data
plt.scatter(X[:, 0], X[:, 1], alpha=0.2)

for length, vector in zip(pca.explained_variance_, pca.components_):
    v = vector * 3 * np.sqrt(length)
    draw_vector(pca.mean_, pca.mean_ + v)
    plt.axis('equal');
    


#%%
pca = PCA(n_components=1)
pca.fit(X)
X_pca = pca.transform(X)
print("original shape:   ", X.shape)
print("transformed shape:", X_pca.shape)

X_new = pca.inverse_transform(X_pca)
plt.scatter(X[:, 0], X[:, 1], alpha=0.2)
plt.scatter(X_new[:, 0], X_new[:, 1], alpha=0.8)
plt.axis('equal');



#%% Question 8 Part A


from sklearn.datasets import fetch_openml

x_data, y_data = fetch_openml('mnist_784', version = 1, return_X_y = True)
#%%
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(
    x_data, y_data, test_size=0.25, random_state=0)

#%%
Zeros = [];
Ones = [];
Twos = [];
Threes = [];
Fours = [];
Fives = [];
Sixes = [];
Sevens = [];
Eights = [];
Nines = [];

#%%


for i in range (0, x_test.shape[0]):
    if y_test[i] == '0' :
        if len(Zeros) < 2:
            Zeros.append(x_test[i]);
    elif y_test[i] == '1' :
        if len(Ones) < 2:
            Ones.append(x_test[i]);
    elif y_test[i] == '2' :
        if len(Twos) < 2:
            Twos.append(x_test[i]);        
    elif y_test[i] == '3' :
        if len(Threes) < 2:
            Threes.append(x_test[i]);        
    elif y_test[i] == '4' :
        if len(Fours) < 2:
            Fours.append(x_test[i]);    
    elif y_test[i] == '5' :
        if len(Fives) < 2:
            Fives.append(x_test[i]);  
    elif y_test[i] == '6' :
        if len(Sixes) < 2:
            Sixes.append(x_test[i]);    
    elif y_test[i] == '7' :
        if len(Sevens) < 2:
            Sevens.append(x_test[i]);    
    elif y_test[i] == '8' :
        if len(Eights) < 2:
            Eights.append(x_test[i]);
    elif y_test[i] == '9' :
        if len(Nines) < 2:
            Nines.append(x_test[i]);   
            

            
#%%

pca = PCA(20)  # project from 64 to 2 dimensions
projected = pca.fit_transform(x_train)
print(x_train.shape)
print(projected.shape)

X_new = pca.inverse_transform(projected)

#%%
from matplotlib import pyplot as plt


ZerosProjected = pca.transform(Zeros)
ZerosReconstructed =  pca.inverse_transform(ZerosProjected)

OnesProjected = pca.transform(Ones)
OnesReconstructed =  pca.inverse_transform(OnesProjected)

TwosProjected = pca.transform(Twos)
TwosReconstructed =  pca.inverse_transform(TwosProjected)

ThreesProjected = pca.transform(Threes)
ThreesReconstructed =  pca.inverse_transform(ThreesProjected)

FoursProjected = pca.transform(Fours)
FoursReconstructed =  pca.inverse_transform(FoursProjected)

FivesProjected = pca.transform(Fives)
FivesReconstructed =  pca.inverse_transform(FivesProjected)

SixesProjected = pca.transform(Sixes)
SixesReconstructed =  pca.inverse_transform(SixesProjected)

SevensProjected = pca.transform(Sevens)
SevensReconstructed =  pca.inverse_transform(SevensProjected)

EightsProjected = pca.transform(Eights)
EightsReconstructed =  pca.inverse_transform(EightsProjected)

NinesProjected = pca.transform(Nines)
NinesReconstructed =  pca.inverse_transform(NinesProjected)



#%%
def plot_digits(data):
    
    fig, axes = plt.subplots(1, 2, figsize=(10, 4),
                             subplot_kw={'xticks':[], 'yticks':[]},
                             gridspec_kw=dict(hspace=0.1, wspace=0.1))
    for i, ax in enumerate(axes.flat):
        ax.imshow(data[i].reshape(28, 28),
                  cmap='binary', interpolation='nearest',
                  clim=(0, 16))
        
#%%        
plot_digits(ZerosReconstructed)
#%%
plot_digits(OnesReconstructed)
#%%
plot_digits(TwosReconstructed)
#%%
plot_digits(ThreesReconstructed)
#%%
plot_digits(FoursReconstructed)
#%%
plot_digits(FivesReconstructed)
#%%
plot_digits(SixesReconstructed)
#%%
plot_digits(SevensReconstructed)
#%%
plot_digits(EightsReconstructed)
#%%
plot_digits(NinesReconstructed)
#%% Part B
from numpy import linalg as LA
from matplotlib import pyplot as plt

Errors = []
NumOfComponents = [10, 20, 30, 40, 50]

def ReconstructedErrorPCA(K):
    pca = PCA(K)  
    projected = pca.fit_transform(x_train)
    
    ZerosProjected = pca.transform(Zeros)
    ZerosReconstructed =  pca.inverse_transform(ZerosProjected)
    
    OnesProjected = pca.transform(Ones)
    OnesReconstructed =  pca.inverse_transform(OnesProjected)
    
    TwosProjected = pca.transform(Twos)
    TwosReconstructed =  pca.inverse_transform(TwosProjected)
    
    ThreesProjected = pca.transform(Threes)
    ThreesReconstructed =  pca.inverse_transform(ThreesProjected)
    
    FoursProjected = pca.transform(Fours)
    FoursReconstructed =  pca.inverse_transform(FoursProjected)
    
    FivesProjected = pca.transform(Fives)
    FivesReconstructed =  pca.inverse_transform(FivesProjected)
    
    SixesProjected = pca.transform(Sixes)
    SixesReconstructed =  pca.inverse_transform(SixesProjected)
    
    SevensProjected = pca.transform(Sevens)
    SevensReconstructed =  pca.inverse_transform(SevensProjected)
    
    EightsProjected = pca.transform(Eights)
    EightsReconstructed =  pca.inverse_transform(EightsProjected)
    
    NinesProjected = pca.transform(Nines)
    NinesReconstructed =  pca.inverse_transform(NinesProjected)
    
    
    
    SumOfErrors = LA.norm(Zeros - ZerosReconstructed, 2) + LA.norm(Ones - OnesReconstructed, 2) + LA.norm(Twos - TwosReconstructed, 2) + LA.norm(Threes - ThreesReconstructed, 2) + LA.norm(Fours - FoursReconstructed, 2) +LA.norm(Fives - FivesReconstructed, 2) + LA.norm(Sixes - SixesReconstructed, 2) +LA.norm(Sevens - SevensReconstructed, 2) + LA.norm(Eights - EightsReconstructed, 2) + LA.norm(Nines - NinesReconstructed, 2) 
    print(SumOfErrors)
    
    Errors.append(SumOfErrors/20)
    

for K in NumOfComponents:
    ReconstructedErrorPCA(K)
    
plt.plot( NumOfComponents, Errors )    
plt.title('Reconstructed Error')    


#%% Part C

ZeroTrain = []
OneTrain = []
TwoTrain = []
ThreeTrain = []
FourTrain = []
FiveTrain = []
SixTrain = []
SevenTrain = []
EightTrain =[]
NineTrain = []

for i in range (0, x_train.shape[0]):
    if y_train[i] == '0' :
        ZeroTrain.append(x_train[i]);
    elif y_train[i] == '1' :
        OneTrain.append(x_train[i]);
    elif y_train[i] == '2' :
        TwoTrain.append(x_train[i]);
    elif y_train[i] == '3' :
        ThreeTrain.append(x_train[i]);
    elif y_train[i] == '4' :
        FourTrain.append(x_train[i]);     
    elif y_train[i] == '5' :
        FiveTrain.append(x_train[i]);
    elif y_train[i] == '6' :
        SixTrain.append(x_train[i]);
    elif y_train[i] == '7' :
        SevenTrain.append(x_train[i]);
    elif y_train[i] == '8' :
        EightTrain.append(x_train[i]);
    elif y_train[i] == '9' :
        NineTrain.append(x_train[i]);


#%%
from matplotlib import pyplot as plt


pca = PCA(40) 
projected = pca.fit_transform(ZeroTrain) 

ZerosProjected = pca.transform(Zeros)
ZerosReconstructed =  pca.inverse_transform(ZerosProjected)
plot_digits(ZerosReconstructed)

#%%
pca = PCA(40) 
projected = pca.fit_transform(OneTrain) 

OnesProjected = pca.transform(Ones)
OnesReconstructed =  pca.inverse_transform(OnesProjected)
plot_digits(OnesReconstructed)

#%%
pca = PCA(40) 
projected = pca.fit_transform(TwoTrain) 

TwosProjected = pca.transform(Twos)
TwosReconstructed =  pca.inverse_transform(TwosProjected)
plot_digits(TwosReconstructed)

#%%
pca = PCA(40) 
projected = pca.fit_transform(ThreeTrain) 

ThreesProjected = pca.transform(Threes)
ThreesReconstructed =  pca.inverse_transform(ThreesProjected)
plot_digits(ThreesReconstructed)

#%%
pca = PCA(40) 
projected = pca.fit_transform(FourTrain) 

FoursProjected = pca.transform(Fours)
FoursReconstructed =  pca.inverse_transform(FoursProjected)
plot_digits(FoursReconstructed)
#%%
pca = PCA(40) 
projected = pca.fit_transform(FiveTrain) 

FivesProjected = pca.transform(Fives)
FivesReconstructed =  pca.inverse_transform(FivesProjected)
plot_digits(FivesReconstructed)

#%%

pca = PCA(40) 
projected = pca.fit_transform(SixTrain) 

SixesProjected = pca.transform(Sixes)
SixesReconstructed =  pca.inverse_transform(SixesProjected)
plot_digits(SixesReconstructed)

#%%

pca = PCA(40) 
projected = pca.fit_transform(SevenTrain) 

SevensProjected = pca.transform(Sevens)
SevensReconstructed =  pca.inverse_transform(SevensProjected)
plot_digits(SevensReconstructed)

#%%

pca = PCA(40) 
projected = pca.fit_transform(EightTrain) 

EightsProjected = pca.transform(Eights)
EightsReconstructed =  pca.inverse_transform(EightsProjected)
plot_digits(EightsReconstructed)

#%%
pca = PCA(40) 
projected = pca.fit_transform(NineTrain) 

NinesProjected = pca.transform(Nines)
NinesReconstructed =  pca.inverse_transform(NinesProjected)
plot_digits(NinesReconstructed)
 
#%% Part D

Errors = []
NumOfComponents = [10, 20, 30, 40, 50]


def ReconstructedErrorPCA(K):
    pca = PCA(K) 
    projected = pca.fit_transform(ZeroTrain) 
    
    ZerosProjected = pca.transform(Zeros)
    ZerosReconstructed =  pca.inverse_transform(ZerosProjected)
    ####
    pca = PCA(K) 
    projected = pca.fit_transform(OneTrain) 
    
    OnesProjected = pca.transform(Ones)
    OnesReconstructed =  pca.inverse_transform(OnesProjected)
    ####
    pca = PCA(K) 
    projected = pca.fit_transform(TwoTrain) 
    
    TwosProjected = pca.transform(Twos)
    TwosReconstructed =  pca.inverse_transform(TwosProjected)
    ####
    pca = PCA(K) 
    projected = pca.fit_transform(ThreeTrain) 
    
    ThreesProjected = pca.transform(Threes)
    ThreesReconstructed =  pca.inverse_transform(ThreesProjected)
    ####
    pca = PCA(K) 
    projected = pca.fit_transform(FourTrain) 
    
    FoursProjected = pca.transform(Fours)
    FoursReconstructed =  pca.inverse_transform(FoursProjected)
    ####
    pca = PCA(K) 
    projected = pca.fit_transform(FiveTrain) 
    
    FivesProjected = pca.transform(Fives)
    FivesReconstructed =  pca.inverse_transform(FivesProjected)
    ####    
    pca = PCA(K) 
    projected = pca.fit_transform(SixTrain) 
    
    SixesProjected = pca.transform(Sixes)
    SixesReconstructed =  pca.inverse_transform(SixesProjected)
    ####
    pca = PCA(K) 
    projected = pca.fit_transform(SevenTrain) 
    
    SevensProjected = pca.transform(Sevens)
    SevensReconstructed =  pca.inverse_transform(SevensProjected)    
    ####
    pca = PCA(K) 
    projected = pca.fit_transform(EightTrain) 
    
    EightsProjected = pca.transform(Eights)
    EightsReconstructed =  pca.inverse_transform(EightsProjected)
    ####
    pca = PCA(K) 
    projected = pca.fit_transform(NineTrain) 
    
    NinesProjected = pca.transform(Nines)
    NinesReconstructed =  pca.inverse_transform(NinesProjected)
        
    
    SumOfErrors = LA.norm(Zeros - ZerosReconstructed, 2) + LA.norm(Ones - OnesReconstructed, 2) + LA.norm(Twos - TwosReconstructed, 2) + LA.norm(Threes - ThreesReconstructed, 2) + LA.norm(Fours - FoursReconstructed, 2) +LA.norm(Fives - FivesReconstructed, 2) + LA.norm(Sixes - SixesReconstructed, 2) +LA.norm(Sevens - SevensReconstructed, 2) + LA.norm(Eights - EightsReconstructed, 2) + LA.norm(Nines - NinesReconstructed, 2) 
    print(SumOfErrors)
    
    Errors.append(SumOfErrors/20)
    

for K in NumOfComponents:
    ReconstructedErrorPCA(K)


plt.figure()    
plt.plot( NumOfComponents, Errors )    
plt.title('Reconstructed Error')    




#%%
from sklearn.decomposition import PCA

pca = PCA().fit(x_train)
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance');