# -*- coding: utf-8 -*-
"""
Created on Fri May 15 00:21:19 2020

@author: Lenovo
"""

from sklearn.model_selection import cross_val_score
from sklearn.svm import LinearSVC
from sklearn.model_selection import GridSearchCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.multiclass import OneVsOneClassifier
#from sklearn.datasets import mnist_train
#from sklearn.datasets import mnist_test
from sklearn.model_selection import cross_val_score
from mnist import MNIST
#%%
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn.datasets import fetch_openml

x_data, y_data = fetch_openml('mnist_784', version = 1, return_X_y = True)

#%%
import numpy as np
x_desired = []
y_desired = []

for i in range(0, x_data.shape[0]):
    if(y_data[i] == '2' or y_data[i] == '3' or y_data[i] == '7'):
        x_desired.append(x_data[i])
        y_desired.append(y_data[i])
        
        
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(
    x_desired, y_desired, test_size=0.5, random_state=0)
        
#(train_data, train_labels), (test_data, test_labels) = tf.keras.datasets.mnist.load_data()
#%%
# One Vs Rest
grid = GridSearchCV(LinearSVC(), {'C': [1.0, 2.0, 4.0, 8.0, 10.0]}) 
grid.fit(x_train, y_train)


print(grid.best_score_)
print(grid.best_params_)

model = OneVsRestClassifier(grid.best_estimator_)


model.fit(x_train, y_train)

print(np.mean(cross_val_score(model, x_desired, y_desired, cv= 5)))

#%%
# One Vs One
grid = GridSearchCV(LinearSVC(), {'C': [1.0, 2.0, 4.0, 8.0, 10.0]}) 
grid.fit(x_train, y_train)


print(grid.best_score_)
print(grid.best_params_)

model = OneVsOneClassifier(grid.best_estimator_)


model.fit(x_train, y_train)

print(np.mean(cross_val_score(model, x_desired, y_desired, cv= 5)))

#%%
from sklearn.svm import SVC

tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-2, 1e-1, 1 , 10],
                     'C': [1, 10, 100, 1000]}]

grid = GridSearchCV(
        SVC(kernel = 'rbf'), tuned_parameters)




#%%
# One Vs Rest
grid.fit(x_train, y_train)


print(grid.best_score_)
print(grid.best_params_)

model = OneVsRestClassifier(grid.best_estimator_)


model.fit(x_train, y_train)

print(np.mean(cross_val_score(model, x_desired, y_desired, cv= 5)))

#%%
# One Vs One
grid.fit(x_train, y_train)


print(grid.best_score_)
print(grid.best_params_)

model = OneVsOneClassifier(grid.best_estimator_)


model.fit(x_train, y_train)

print(np.mean(cross_val_score(model, x_desired, y_desired, cv= 5)))

#%%

    clf.fit(x_train, y_train)

    print("Best parameters set found on development set:")
    print()
    print(clf.best_params_)
    print()
    print("Grid scores on development set:")
    print()
    means = clf.cv_results_['mean_test_score']
    stds = clf.cv_results_['std_test_score']
    for mean, std, params in zip(means, stds, clf.cv_results_['params']):
        print("%0.3f (+/-%0.03f) for %r"
              % (mean, std * 2, params))
    print()

    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()
    y_true, y_pred = y_test, clf.predict(X_test)
    print(classification_report(y_true, y_pred))
    print()

