﻿## HW4
### Splines and SVM

This Homework contains several theoretical questions about cubic splines and kernel SVMs.
It also contains implementation of soft margin linear SVM and soft margin kernel SVM using Gaussian kernel on MNIST dataset with the help of cross validation for optimizing hyperparameters.

