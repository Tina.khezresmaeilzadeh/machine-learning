﻿## Project
### Applications of Machine Learrning in Stocks Market

The aim of the project is to predict prices of stocks using technical analysis with the help of several indicators. The project contains several methods which are compared in the last sections. The implemented methods are linear regression, k-nearest neighbours, ARIMA, prophet method, SVM and decision tree. 

