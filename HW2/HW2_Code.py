# -*- coding: utf-8 -*-
"""
HW 2 
An Introduction To Machine Learning
    Dr. Mohammadzadeh

@author: Tina KhezrEsmaeilzadeh
    96101595
"""

import numpy as np
import pandas as pd
from sklearn.utils import shuffle
from sklearn.linear_model import LinearRegression

# Alef
data = pd.read_csv('data.csv')


#Shuffled_Data = data.sample(frac = 1)
#Shuffled_Data_2=random.shuffle(data)



Shuffled_Data = shuffle(data)
Shuffled_Data.to_csv('Shuffled_Data.csv')
#%%
TotalData=Shuffled_Data[Shuffled_Data.columns[:-1]]
TotalLabel=Shuffled_Data.SalePrice

TotalData.to_csv('TotalData.csv')
TotalLabel.to_csv('TotalLabel.csv')

NumberOfData, NumberOfFeatures = TotalData.shape

#%% Be
x = np.zeros(1200)
for i in range(600):
    x[i] = 1
bool_arr = np.array(x, dtype=bool)    

#%%
TestLabel = TotalLabel[bool_arr]
TestData = TotalData[bool_arr]

TrainData = TotalData[~bool_arr]
TrainLabel = TotalLabel[~bool_arr]

#%%
from sklearn.linear_model import LinearRegression
linear_regression = LinearRegression()
linear_regression.fit(TrainData, TrainLabel)

print("The intercept is",linear_regression.intercept_)
print("The Coefficients are ", linear_regression.coef_.tolist())

print("The Mean Squared Error for Train Data is ", linear_regression.score(TrainData, TrainLabel ))
print("The Mean Squared Error for Test data is ", linear_regression.score(TestData, TestLabel) )



import statsmodels.api as sm


model1 = sm.OLS( TrainLabel, TrainData )
result = model1.fit()
#%%
print(result.summary())

#%% Jim
from matplotlib import pyplot as plt
def Choosing_Train_Test_Datasets(n, TotalData, TotalLabel):
    x = np.zeros(len(TotalData))
    for i in range(n):
        x[i + 600] = 1
        bool_arr = np.array(x, dtype=bool)    


    TestLabel = TotalLabel[~bool_arr]
    TestData = TotalData[~bool_arr]

    TrainData = TotalData[bool_arr]
    TrainLabel = TotalLabel[bool_arr]
    return TestLabel, TestData, TrainData, TrainLabel

MSE_Error_Train = []
MSE_Error_Test = []

N = [20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 300, 400, 500, 600]
for n in N:
    
    TestLabel1, TestData1, TrainData1, TrainLabel1 = Choosing_Train_Test_Datasets(n, TotalData, TotalLabel)
    linear_regression = LinearRegression()
    linear_regression.fit(TrainData1, TrainLabel1)
    MSE_Error_Train.append(np.mean( (linear_regression.predict(TrainData1) - TrainLabel1)**2   ))
    MSE_Error_Test.append(np.mean( (linear_regression.predict(TestData1) - TestLabel1)**2   ))
    
plt.plot(N, MSE_Error_Train)
plt.xlabel('Number Of Train Data')    
plt.ylabel('Number Of Test Data')
plt.title('MSE Error Of Train Data')

plt.figure()    
plt.plot(N, MSE_Error_Test)
plt.xlabel('Number Of Train Data')    
plt.ylabel('Number Of Test Data')
plt.title('MSE Error Of Test Data')

#%% Dal
# p values for each variable in Multiple Linear Regression
import statsmodels.api as sm
model1 = sm.OLS( TrainLabel, TrainData )
result = model1.fit()
print(result.summary())
# p values for each variable in Linear Regression
TotalData_Id = TotalData.Id
TotalData_LotFrontage = TotalData.LotFrontage
TotalData_LotArea = TotalData.LotArea
TotalData_YearBuilt = TotalData.YearBuilt
TotalData_BedroomAbvGr = TotalData.BedroomAbvGr
TotalData_KitchenAbvGr = TotalData.KitchenAbvGr
TotalData_YrSold = TotalData.YrSold


x = np.zeros(1200)
for i in range(600):
    x[i] = 1
bool_arr = np.array(x, dtype=bool)    


TestLabel = TotalLabel[bool_arr]
TestData_Id = TotalData_Id[bool_arr]
TestData_LotFrontage = TotalData_LotFrontage[bool_arr]
TestData_LotArea = TotalData_LotArea[bool_arr]
TestData_YearBuilt = TotalData_YearBuilt[bool_arr]
TestData_BedroomAbvGr = TotalData_BedroomAbvGr[bool_arr]
TestData_KitchenAbvGr = TotalData_KitchenAbvGr[bool_arr]
TestData_YrSold = TotalData_YrSold[bool_arr]

#%%
TrainLabel = TotalLabel[~bool_arr]
TrainData_Id = TotalData_Id[~bool_arr]

model1 = sm.OLS( TrainLabel, TrainData_Id )
result = model1.fit()
print(result.summary())
#%%

TrainData_LotFrontage = TotalData_LotFrontage[~bool_arr]

model1 = sm.OLS( TrainLabel, TrainData_LotFrontage )
result = model1.fit()
print(result.summary())
#%%
TrainData_LotArea = TotalData_LotArea[~bool_arr]

model1 = sm.OLS( TrainLabel, TrainData_LotArea )
result = model1.fit()
print(result.summary())
#%%
TrainData_YearBuilt = TotalData_YearBuilt[~bool_arr]

model1 = sm.OLS( TrainLabel, TrainData_YearBuilt )
result = model1.fit()
print(result.summary())
#%%
TrainData_BedroomAbvGr = TotalData_BedroomAbvGr[~bool_arr]

model1 = sm.OLS( TrainLabel, TrainData_BedroomAbvGr )
result = model1.fit()
print(result.summary())
#%%
TrainData_KitchenAbvGr = TotalData_KitchenAbvGr[~bool_arr]

model1 = sm.OLS( TrainLabel, TrainData_KitchenAbvGr )
result = model1.fit()
print(result.summary())
#%%
TrainData_YrSold = TotalData_YrSold[~bool_arr]
model1 = sm.OLS( TrainLabel, TrainData_YrSold )
result = model1.fit()
print(result.summary())
#%%
TotalData=Shuffled_Data[Shuffled_Data.columns[:-1]]
TotalData = pd.DataFrame(TotalData,columns=['Id','LotFrontage','LotArea','YearBuilt','BedroomAbvGr','KitchenAbvGr','YrSold'])
#TotalData['LotFrontage&LotArea'] = TotalData['LotFrontage']*TotalData['LotArea']
TotalData['BedroomAbvGr&KitchenAbvGr'] = TotalData['BedroomAbvGr']*TotalData['KitchenAbvGr']
TotalData['BedroomAbvGr&Id'] = TotalData['BedroomAbvGr']*TotalData['Id']
#TotalData['Id&KitchenAbvGr'] = TotalData['Id']*TotalData['KitchenAbvGr']

TotalLabel=Shuffled_Data.SalePrice


x = np.zeros(1200)
for i in range(600):
    x[i] = 1
bool_arr = np.array(x, dtype=bool)    


TestLabel = TotalLabel[bool_arr]
TestData = TotalData[bool_arr]

TrainData = TotalData[~bool_arr]
TrainLabel = TotalLabel[~bool_arr]


from sklearn.linear_model import LinearRegression
linear_regression = LinearRegression()
linear_regression.fit(TrainData, TrainLabel)

print("The intercept is",linear_regression.intercept_)
print("The Coefficients are ", linear_regression.coef_.tolist())

print("The Mean Squared Error for Train Data is ", linear_regression.score(TrainData, TrainLabel ))
print("The Mean Squared Error for Test data is ", linear_regression.score(TestData, TestLabel) )




