﻿## HW3
### Classification Implementation

This Homework contains several theoretical questions about ML and MAP estimation, derivatives of log-likelihood functions, logistic regression and LDA classifier.
It also contains implementation of QDA ،LDA ، KNN and also logistic regression using difits dataset with the help of cross validation.

