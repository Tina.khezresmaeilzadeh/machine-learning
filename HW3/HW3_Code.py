# -*- coding: utf-8 -*-
"""
HW3
Machine Learning
Tina KHezr Esmaeilzadeh

This is a temporary script file.
"""

#%% Question 8


from sklearn.datasets import load_digits
from matplotlib import pyplot as plt

for i in range(10):
    plt.figure
    plt.gray()
    plt.matshow(load_digits().images[i])
    plt.show()
    plt.title(i)
 

#%% Be
    
# Logistic Regression
    
    
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression 
from sklearn.model_selection import cross_val_score
import numpy as np
digits = load_digits()
x_train, x_test, y_train, y_test = train_test_split(digits.data, digits.target, test_size=0.25, random_state=0)
logisticRegr = LogisticRegression(penalty='l1',dual=False,max_iter=110, solver='liblinear' )
logisticRegr.fit(x_train, y_train)

## Checking
if logisticRegr.predict(x_test[0].reshape(1,-1)) == y_test[0]:
    print("Correct")
else:
    print("Not Correct")

predictions = logisticRegr.predict(x_test)    
## Getting the score of this method
score = logisticRegr.score(x_test, y_test)

print(score)    
from sklearn import metrics
print(metrics.confusion_matrix(y_test, predictions))
print(metrics.classification_report(y_test, predictions))
print(np.mean(cross_val_score(logisticRegr, digits.data, digits.target, cv= 5)))


#%% 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score

import numpy as np
# KNN

# I take 75% of data as train data and 25% as test data.
x_train, x_test, y_train, y_test = train_test_split(digits.data, digits.target, test_size=0.25, random_state=0)


# I take 10% of the training data and use that for validation
x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=0.1, random_state=0)

kVals = range(1, 40, 1)
accuracies = []
for k in range(1, 40, 1):
	# train the k-Nearest Neighbor classifier with the current value of `k`
	model = KNeighborsClassifier(n_neighbors=k)
	model.fit(x_train, y_train)
 
	# evaluate the model and update the accuracies list
	score = model.score(x_val, y_val)
	print(" for k=%d, we have accuracy=%f" % (k, score * 100))
	accuracies.append(score)
 
# finding best K
i = int(np.argmax(accuracies))
print("k=%d has the best accuracy" % (kVals[i]))

# Now we can use this k for training
model = KNeighborsClassifier(n_neighbors=kVals[i])
model.fit(x_train, y_train)
predictions = model.predict(x_test)
 
from sklearn import metrics
print(classification_report(y_test, predictions))
print(metrics.confusion_matrix(y_test, predictions))
print(np.mean(cross_val_score(model, digits.data, digits.target, cv= 5)))
#%% LDA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

digits = load_digits()
x_train, x_test, y_train, y_test = train_test_split(digits.data, digits.target, test_size=0.99, random_state=0)
lda = LinearDiscriminantAnalysis()
lda.fit(x_train, y_train)


predictions = lda.predict(x_test)    
## Getting the score of this method
score = lda.score(x_test, y_test)

print(score)    
from sklearn import metrics
print(metrics.confusion_matrix(y_test, predictions))
print(metrics.classification_report(y_test, predictions))
print(np.mean(cross_val_score(lda, digits.data, digits.target, cv= 5)))


#%% QDA
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis


digits = load_digits()
x_train, x_test, y_train, y_test = train_test_split(digits.data, digits.target, test_size=0.25, random_state=0)
qda = QuadraticDiscriminantAnalysis()
qda.fit(x_train, y_train)


predictions = qda.predict(x_test)    
## Getting the score of this method
score = qda.score(x_test, y_test)

print(score)    
from sklearn import metrics
print(metrics.confusion_matrix(y_test, predictions))
print(metrics.classification_report(y_test, predictions))
print(np.mean(cross_val_score(qda, digits.data, digits.target, cv= 5)))




   






